document.addEventListener('DOMContentLoaded', () => {
    document
      .querySelector('#add-form')
      .addEventListener('submit', agregar);
  });

const agregar = x =>
{
    x.preventDefault();

    const li = document.createElement("li");
    var inputValue = document.getElementById("nuevoRegistro").value;
    li.appendChild(document.createTextNode(inputValue));
    li.className="list-group-item";
    document.getElementById("myUL").appendChild(li);
    
    document.getElementById("nuevoRegistro").value = "";
}

